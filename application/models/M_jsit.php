<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_jsit extends CI_Model
{

	// login
	function proseslogin($user, $pass)
	{
		$this->db->where('username', $user);
		$this->db->where('password', $pass);
		return $this->db->get('user')->row();
	}

	// Beranda
	function create_beranda($data)
	{
		$this->db->insert('config_beranda', $data);
	}
	function read_beranda()
	{
		return $this->db->query("SELECT * FROM config_beranda");
	}
	function edit_beranda($id)
	{
		$this->db->where("id_cb", $id);
		return $this->db->get('config_beranda');
	}
	function update_beranda($id, $data)
	{
		$this->db->where("id_cb", $id);
		$this->db->update('config_beranda', $data);
	}
	function delete_beranda($id)
	{
		$this->db->where("id_cb", $id);
		$query = $this->db->get('config_beranda');
		$row = $query->row();
		unlink("upload/config_beranda/$row->background");
		$this->db->delete('config_beranda', array('id_cb' => $id));
	}

	// Tentang
	function create_tentang($data)
	{
		$this->db->insert('config_tentang', $data);
	}
	function read_tentang()
	{
		return $this->db->query("SELECT * FROM config_tentang");
	}
	function edit_tentang($id)
	{
		$this->db->where("id_ct", $id);
		return $this->db->get('config_tentang');
	}
	function update_tentang($id, $data)
	{
		$this->db->where("id_ct", $id);
		$this->db->update('config_tentang', $data);
	}
	function delete_tentang($id)
	{
		$this->db->where("id_ct", $id);
		$query = $this->db->get('config_tentang');
		$row = $query->row();
		unlink("upload/config_tentang/$row->cover_halaman");
		unlink("upload/config_tentang/$row->foto_perusahaan");
		$this->db->delete('config_tentang', array('id_ct' => $id));
	}

	// Kontak
	function create_kontak($data)
	{
		$this->db->insert('config_kontak', $data);
	}
	function read_kontak()
	{
		return $this->db->query("SELECT * FROM config_kontak");
	}
	function edit_kontak($id)
	{
		$this->db->where("id_ck", $id);
		return $this->db->get('config_kontak');
	}
	function update_kontak($id, $data)
	{
		$this->db->where("id_ck", $id);
		$this->db->update('config_kontak', $data);
	}
	function delete_kontak($id)
	{
		$this->db->where("id_ck", $id);
		$query = $this->db->get('config_kontak');
		$row = $query->row();
		unlink("upload/config_kontak/$row->cover_halaman");
		$this->db->delete('config_kontak', array('id_ck' => $id));
	}

	// Produk
	function create_produk($data)
	{
		$this->db->insert('produk', $data);
	}
	function read_produk()
	{
		return $this->db->query("SELECT * FROM produk");
	}
	function edit_produk($id)
	{
		$this->db->where("id_produk", $id);
		return $this->db->get('produk');
	}
	function update_produk($id, $data)
	{
		$this->db->where("id_produk", $id);
		$this->db->update('produk', $data);
	}
	function delete_produk($id)
	{
		$this->db->where("id_produk", $id);
		$query = $this->db->get('produk');
		$row = $query->row();
		unlink("upload/produk/$row->foto_produk");
		$this->db->delete('produk', array('id_produk' => $id));
	}

	// Kelas
	function create_kelas($data)
	{
		$this->db->insert('kelas', $data);
	}
	function read_kelas()
	{
		return $this->db->query("SELECT * FROM kelas");
	}
	function edit_kelas($id)
	{
		$this->db->where("id_kelas", $id);
		return $this->db->get('kelas');
	}
	function update_kelas($id, $data)
	{
		$this->db->where("id_kelas", $id);
		$this->db->update('kelas', $data);
	}
	function delete_kelas($id)
	{
		$this->db->where("id_kelas", $id);
		$this->db->delete('kelas', array('id_kelas' => $id));
	}

	// Kategori
	function create_kategori($data)
	{
		$this->db->insert('kategori', $data);
	}
	function read_kategori()
	{
		return $this->db->query("SELECT * FROM kategori");
	}
	function edit_kategori($id)
	{
		$this->db->where("id_kategori", $id);
		return $this->db->get('kategori');
	}
	function update_kategori($id, $data)
	{
		$this->db->where("id_kategori", $id);
		$this->db->update('kategori', $data);
	}
	function delete_kategori($id)
	{
		$this->db->where("id_kategori", $id);
		$this->db->delete('kategori', array('id_kategori' => $id));
	}

	// Pesanan
	function read_pesanan()
	{
		return $this->db->query("SELECT *, keranjang.id_user as id_user_beli FROM beli join detail_beli USING(id_beli) join keranjang using(id_keranjang) join user using(id_user) join produk using(id_produk) join pembayaran using(id_beli) JOIN member using(id_member) JOIN kelas using(id_kelas) JOIN kategori using(id_kategori) LEFT OUTER JOIN provinsi ON provinsi.id_provinsi=member.provinsi_sekolah GROUP BY id_beli");
	}
	function read_pesanan_total_baru()
	{
		$id_user=$this->input->post('id_user');
		return $this->db->query("SELECT SUM(total_beli) as total FROM beli where id_user = $id_user");
	}
	function update_pesanan(){
		$id_beli=$this->input->post('id_beli');
		$status=$this->input->post('status');

		$this->db->set('status', $status);
		$this->db->where('id_beli', $id_beli);
		$result=$this->db->update('beli');
		return $result;
	}
	function update_pesanan_bayar(){
		$id_beli=$this->input->post('id_beli');
		$status_bayar=$this->input->post('status_bayar');

		$this->db->set('status_bayar', $status_bayar);
		$this->db->where('id_beli', $id_beli);
		$result=$this->db->update('pembayaran');
		return $result;
	}

	// member
	function read_member()
	{
		return $this->db->query("SELECT * FROM member");
	}

	// komplain
	function read_komplain()
	{
		return $this->db->query("SELECT * FROM komplain join detail_beli using(id_beli) join keranjang using(id_keranjang) join user using(id_user) GROUP BY komplain.tanggal_komplain,user.id_user");
	}

	// komplain
	function read_komplain_per_pesanan()
	{
		return $this->db->query("SELECT * FROM komplain join detail_beli using(id_beli) join keranjang using(id_keranjang) join user using(id_user)");
	}
}
