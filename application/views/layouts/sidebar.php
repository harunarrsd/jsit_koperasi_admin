<aside class="main-sidebar">
	<!-- sidebar: style can be found in sidebar.less -->
	<section class="sidebar">
		<!-- sidebar menu: : style can be found in sidebar.less -->
		<ul class="sidebar-menu">
			<li class="header">MAIN NAVIGATION</li>
			<?php if ($this->session->userdata('role') == '1') {
				?>
				<li class="<?php if ($this->uri->segment(1) == 'home') {
												echo 'active open';
											} ?>">
					<a href="<?php echo site_url("home") ?>">
						<i class="fa fa-dashboard"></i>
						<span>Dashboard</span>
					</a>
				</li>
			<?php
			} else if ($this->session->userdata('role') == '3') {
				?>
				<li class="<?php if ($this->uri->segment(1) == 'home_p') {
												echo 'active open';
											} ?>">
					<a href="<?php echo site_url("home_p") ?>">
						<i class="fa fa-dashboard"></i>
						<span>Dashboard</span>
					</a>
				</li>
			<?php
			}
			?>
			<li class="header">DATA CORE</li>
			<?php if ($this->session->userdata('role') == '1') {
				?>
				<!-- <li class="<?php if ($this->uri->segment(1) == 'akun') {
												echo 'active open';
											} ?>">
					<a href="<?php echo site_url("akun") ?>">
						<i class="fa fa-newspaper-o"></i>
						<span>Akun</span>
					</a>
				</li>
				<li class="treeview <?php if ($this->uri->segment(1) == 'sekolah') {
																echo 'active open';
															} ?>">
					<a href="#">
						<i class="fa fa-user"></i>
						<span>Sekolah</span>
						<span class="pull-right-container">
							<i class="fa fa-angle-left pull-right"></i>
						</span>
					</a>
					<ul class="treeview-menu">
						<li><a href="<?php echo site_url("sekolah/member") ?>"><i class="fa fa-circle"></i> Member</a></li>
						<li><a href="<?php echo site_url("sekolah/non_member") ?>"><i class="fa fa-circle"></i> Non Member</a></li>
					</ul>
				</li> -->
				<li class="<?php if ($this->uri->segment(1) == 'pesanan') {
												echo 'active open';
											} ?>">
					<a href="<?php echo site_url("pesanan") ?>">
						<i class="fa fa-newspaper-o"></i>
						<span>Pesanan</span>
					</a>
				</li>
				<li class="<?php if ($this->uri->segment(1) == 'komplain') {
												echo 'active open';
											} ?>">
					<a href="<?php echo site_url("komplain") ?>">
						<i class="fa fa-envelope"></i>
						<span>Komplain</span>
					</a>
				</li>
				<li class="treeview <?php if ($this->uri->segment(1) == 'core_produk') {
																echo 'active open';
															} ?>">
					<a href="#">
						<i class="fa fa-user"></i>
						<span>Produk</span>
						<span class="pull-right-container">
							<i class="fa fa-angle-left pull-right"></i>
						</span>
					</a>
					<ul class="treeview-menu">
						<li><a href="<?php echo site_url("core_produk/Kelas") ?>"><i class="fa fa-circle"></i> Kelas</a></li>
						<li><a href="<?php echo site_url("core_produk/Kategori") ?>"><i class="fa fa-circle"></i> Kategori</a></li>
					</ul>
				</li>
				<li class="header">CONFIG</li>
				<li class="<?php if ($this->uri->segment(1) == 'config_beranda') {
												echo 'active open';
											} ?>">
					<a href="<?php echo site_url("config_beranda") ?>">
						<i class="fa fa-file-text"></i>
						<span>Beranda</span>
					</a>
				</li>
				<li class="<?php if ($this->uri->segment(1) == 'produk') {
												echo 'active open';
											} ?>">
					<a href="<?php echo site_url("produk") ?>">
						<i class="fa fa-sticky-note"></i>
						<span>Produk</span>
					</a>
				</li>
				<li class="<?php if ($this->uri->segment(1) == 'config_tentang') {
												echo 'active open';
											} ?>">
					<a href="<?php echo site_url("config_tentang") ?>">
						<i class="fa fa-file"></i>
						<span>Tentang</span>
					</a>
				</li>
				<li class="<?php if ($this->uri->segment(1) == 'config_kontak') {
												echo 'active open';
											} ?>">
					<a href="<?php echo site_url("config_kontak") ?>">
						<i class="fa fa-file"></i>
						<span>Kontak</span>
					</a>
				</li>
			<?php
			} else if ($this->session->userdata('role') == '3') {
				?>
				<li class="<?php if ($this->uri->segment(1) == 'pesanan') {
												echo 'active open';
											} ?>">
					<a href="<?php echo site_url("pesanan") ?>">
						<i class="fa fa-newspaper-o"></i>
						<span>Pesanan</span>
					</a>
				</li>
				<li class="<?php if ($this->uri->segment(1) == 'komplain') {
												echo 'active open';
											} ?>">
					<a href="<?php echo site_url("komplain") ?>">
						<i class="fa fa-envelope"></i>
						<span>Komplain</span>
					</a>
				</li>
				<li class="<?php if ($this->uri->segment(1) == 'produk') {
												echo 'active open';
											} ?>">
					<a href="<?php echo site_url("produk") ?>">
						<i class="fa fa-sticky-note"></i>
						<span>Produk</span>
					</a>
				</li>
			<?php
			}
			?>
		</ul>
	</section>
	<!-- /.sidebar -->
</aside>
