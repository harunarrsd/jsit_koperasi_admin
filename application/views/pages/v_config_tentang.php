<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
      Config Tentang
    </h1>
    <ol class="breadcrumb">
      <li><a href="<?php echo site_url("home")?>"><i class="fa fa-home"></i> Home</a></li>
      <li>Config</li>
      <li class="active">Tentang</li>
    </ol><br>
    <?php echo $this->session->flashdata('notif')?>
</section>

  <!-- Main content -->
<section class="content">
    <!-- Info boxes -->
    <div class="box">
        <div class="box-header">
            <a href="<?php echo site_url('config_tentang/form')?>" class="btn btn-hajj"><i class="fa fa-plus"></i> Tambah </a>
        </div>
        <div class="box-body">
            <table id="example1" class="table table-striped table-bordered">
                <thead>
                    <tr>
                        <th>No.</th>
                        <th>Judul Halaman</th>
                        <th>Cover Halaman</th>
                        <th>Judul Tentang</th>
                        <th>Isi Tentang</th>
                        <th>Nama Quotes</th>
                        <th>Isi Quotes</th>
                        <th>Foto Perusahaan</th>
                        <th width="91">Action</th>
                    </tr>
                </thead>

                <tbody>
                <?php
                    $no=0;
                    foreach ($main['sql']->result() as $obj)
                    {
                        $id = $obj->id_ct;
                        $no++;
                ?>
                    <tr>
                        <td><?php echo $no;?></td>
                        <td><?php echo $obj->judul_halaman;?></td>
						<td>
							<a href="#" class="btn btn-xs" data-toggle="modal" data-target=".gambar<?php echo $id;?>">
								<i class='fa fa-eye'></i> Lihat Gambar
                            </a>
						</td>
						<td><?php echo $obj->judul_tentang;?></td>
						<td><?php echo word_limiter($obj->isi_tentang, 25);?></td>
						<td><?php echo $obj->nama_quotes;?></td>
						<td><?php echo word_limiter($obj->isi_quotes, 25);?></td>
						<td>
							<a href="#" class="btn btn-xs" data-toggle="modal" data-target=".gambar2<?php echo $id;?>">
								<i class='fa fa-eye'></i> Lihat Gambar
                            </a>
						</td>
                        <td>
                            <a class="btn btn-xs btn-info" href="<?php echo site_url();?>config_tentang/form_edit/<?php echo $id;?>"><i class='fa fa-edit'></i></a>
                            <a  class="btn btn-xs btn-danger" href="javascript:if(confirm('Apakah Anda yakin ?')){document.location='<?php echo site_url();?>/config_tentang/delete/<?php echo $id;?>';}"><i class='fa fa-trash'></i></a>
                        </td>
                    </tr>
                <?php
                    }
                ?>
                </tbody>
            </table>
        </div>
    </div>
</section>

<!-- Modal Ganti Password -->
<?php
    foreach ($main['sql']->result() as $obj)
    {
        $id = $obj->id_ct;
?>
<div class="modal fade gambar<?php echo $id;?>" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
  <div class="modal-dialog modal-md" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      </div>
      <div class="modal-body">
        <img src="<?php if($obj->cover_halaman==NULL) echo site_url('assets/images/dummy.png'); else echo site_url("upload/config_tentang/$obj->cover_halaman");?>" width="100%">
      </div>
    </div>
  </div>
</div>
<?php
}
?>

<?php
    foreach ($main['sql']->result() as $obj)
    {
        $id = $obj->id_ct;
?>
<div class="modal fade gambar2<?php echo $id;?>" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
  <div class="modal-dialog modal-md" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      </div>
      <div class="modal-body">
        <img src="<?php if($obj->foto_perusahaan==NULL) echo site_url('assets/images/dummy.png'); else echo site_url("upload/config_tentang/$obj->foto_perusahaan");?>" width="100%">
      </div>
    </div>
  </div>
</div>
<?php
}
?>
