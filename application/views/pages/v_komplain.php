<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
      Komplain
    </h1>
    <ol class="breadcrumb">
      <li><a href="<?php echo site_url("home")?>"><i class="fa fa-home"></i> Home</a></li>
      <li class="active">Komlain</li>
    </ol><br>
    <?php echo $this->session->flashdata('notif')?>
</section>

  <!-- Main content -->
<section class="content">
    <!-- Info boxes -->
    <div class="box">
        <div class="box-header">
            <!-- <a href="<?php echo site_url('config_kontak/form')?>" class="btn btn-hajj"><i class="fa fa-plus"></i> Tambah </a> -->
        </div>
        <div class="box-body">
						<!-- table -->
						<table id="myData" class="table table-striped table-bordered">
							<thead>
								<tr>
									<th>No.</th>
									<th>Nama Pemesan</th>
									<th>Pesan Komplain</th>
									<th>Tanggal Komplain</th>
								</tr>
							</thead>

							<tbody id="show_komplain">
							</tbody>
						</table>
        </div>
    </div>
</section>

<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery-3.2.1.js"></script>
<script src="<?php echo base_url();?>assets/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url();?>assets/plugins/datatables/dataTables.bootstrap.min.js"></script>
<script type="text/javascript">
$(document).ready(function(){
	show_komplain();

	$("#myData").dataTable();

	function show_komplain(){
		$.ajax({
			url   : '<?php echo site_url('komplain/data_komplain')?>',
			async : false,
			dataType : 'json',
			success : function(data){
				var html = '';
				var i;
				for(i=0; i<data.length; i++){
					var j = i+1;
					if(data[i].status_bayar == 'Sudah'){
						var kirim_invoice = '<a class="btn btn-xs btn-success item_invoice" data-id_beli="'+data[i].id_beli+'" data-nama_pemesan="'+data[i].nama_user+'"><i class="fa fa-edit" title="edit"></i> Kirim Invoice</a>';
					}else{
						var kirim_invoice = '';
					}
					html += 
					'<tr>'+
						'<td>'+j+'</td>'+
						'<td>'+data[i].nama_user+'</td>'+
						'<td>'+data[i].pesan+'</td>'+
						'<td>'+data[i].tanggal_komplain+'</td>'+
					'</tr>';
				}
				$('#show_komplain').html(html);
			}

		});
	}
});
</script>
