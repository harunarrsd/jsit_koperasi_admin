<!-- Content Header (Page header) -->
<section class="content-header">
	<h1>
		Pesanan
	</h1>
	<ol class="breadcrumb">
		<li><a href="<?php echo site_url("home") ?>"><i class="fa fa-home"></i> Home</a></li>
		<li class="active">Pesanan</li>
	</ol><br>
	<?php echo $this->session->flashdata('notif') ?>
</section>
<?php
function convert_to_rupiah($angka)
	{
		return 'Rp. '.strrev(implode('.',str_split(strrev(strval($angka)),3)));
	}
?>
<!-- Main content -->
<section class="content">
	<!-- Info boxes -->
	<div class="box">
		<div class="box-header">
			<!-- <a href="<?php echo site_url('config_kontak/form') ?>" class="btn btn-hajj"><i class="fa fa-plus"></i> Tambah </a> -->
		</div>
		<div class="box-body table-responsive">
			<!-- table -->
			<table id="myData" class="table table-bordered table-hover">
				<thead>
				<tr>
						<th rowspan="2">No.</th>
						<th rowspan="2">Nama Sekolah</th>
						<th rowspan="2">Alamat</th>
						<th rowspan="2">Provinsi</th>
						<th rowspan="2">Nama Contact Person</th>
						<th rowspan="2">Nomor Handphone CP</th>
						<!--<th>Produk</th>-->
						<?php
						$data_kelas = $this->db->query('SELECT * from kelas');
    					$data_kategori = $this->db->query('SELECT * from kategori');
    					$num_kategori=count($data_kategori->result());
						foreach($data_kelas->result() as $sql2){
						?>
						<th colspan="<?php echo $num_kategori;?>"><?php echo $sql2->nama_kelas;?></th>
						<th rowspan="2">Total</th>
						<?php
						}
						?>
						<!--<th>Jumlah</th>-->
						<th rowspan="2">Jumlah Total</th>
						<th rowspan="2">Harga Total</th>
						<th rowspan="2">Bonus</th>
						<th rowspan="2">Status Pesanan</th>
						<th rowspan="2">Status Bayar</th>
						<th rowspan="2">Bukti Bayar</th>
						<th rowspan="2" width="91">Action</th>
					</tr>
					<tr>
						<!--<th>Produk</th>-->
						<?php
						foreach($data_kelas->result() as $trash){
    						foreach($data_kategori->result() as $sql2){
        						?>
        						<th><?php echo $sql2->nama_kategori;?></th>
        						<?php
    						}
    						?>
						<?php
						}
						?>
					</tr>
				</thead>

				<tbody>
				<?php 
				$no=0;
					foreach($main['sql'] as $sql){
						$no++;
				?>
				<tr>
					<td><?php echo $no;?></td>
					<td><?php echo $sql->nama_user;?></td>
					<td><?php echo $sql->alamat_sekolah;?></td>
					<td><?php echo $sql->nama_provinsi;?></td>
					<td><?php echo $sql->cp;?></td>
					<td><?php echo $sql->no_handphone;?></td>
					<!--<td>-->
					<!--<?php
					$data2 = $this->db->query('SELECT *, keranjang.id_user as id_user_beli FROM detail_beli JOIN beli using(id_beli) join keranjang using(id_keranjang) JOIN produk using(id_produk) RIGHT OUTER JOIN kelas using(id_kelas) RIGHT OUTER JOIN kategori using(id_kategori) where keranjang.id_user = "'.$sql->id_user_beli.'" AND detail_beli.id_beli = "'.$sql->id_beli.'" ');
					$num=0;
					foreach($data2->result() as $sql2){
				 	?>-->
				<!-- 	<?php if($num!=0)echo ',<br><br>'; echo $sql2->judul_produk; ?>-->
				 <!-- 	<?php 
				 	$num++;
			     	} 
					?>-->
					<!--</td>-->
					<!--<td>
					<?php 
					$num=0;
					foreach($data2->result() as $sql2){
					?>
					<?php if($num!=0)echo ',<br><br>'; echo $sql2->nama_kelas;?>
					<?php
					$num++;
					}
					?>
					</td>
					<td>
					<?php 
					$num=0;
					foreach($data2->result() as $sql2){
					?>
					<?php if($num!=0)echo ',<br><br>'; echo $sql2->nama_kategori;?>
					<?php
					$num++;
					}
					?>
					</td>
					<td>
					<?php 
					$num=0;
					foreach($data2->result() as $sql2){
					?>
					<?php if($num!=0)echo ',<br><br>'; echo $sql2->jumlah;?>
					<?php
					$num++;
					}
					?>	-->
					<?php
					$data2 = $this->db->query('SELECT *, keranjang.id_user as id_user_beli FROM detail_beli JOIN beli using(id_beli) join keranjang using(id_keranjang) JOIN produk using(id_produk) RIGHT OUTER JOIN kelas using(id_kelas) RIGHT OUTER JOIN kategori using(id_kategori) where keranjang.id_user = "'.$sql->id_user_beli.'" AND detail_beli.id_beli = "'.$sql->id_beli.'" ');
					$data_kelas = $this->db->query('SELECT * from kelas');
					$data_kategori = $this->db->query('SELECT * from kategori');
					$num_kategori=count($data_kategori->result());
					foreach($data_kelas->result() as $i_kelas){
					    $total_produk=0;
    					foreach($data_kategori->result() as $i_kategori){
					    ?>
    					<td>
    					<?php
        					$jumlah_produk_sama=0;
        					foreach($data2->result() as $sql2){
        					    if($sql2->id_kelas==$i_kelas->id_kelas){
            					    if($sql2->id_kategori==$i_kategori->id_kategori){
                					   $jumlah_produk_sama = $jumlah_produk_sama+floatval($sql2->jumlah);
            					        $total_produk=$total_produk+floatval($sql2->jumlah);
            					    }
        					    }
        					}
        					
        					 echo $jumlah_produk_sama;
    					?>
    					</td>
    					<?php
    					}
					?>
    					<th><?php echo $total_produk?></th>
					<?php
					}
					?>
					</td>
					<td><?php echo $sql->jumlah_total;?></td>
					<td><?php echo convert_to_rupiah($sql->total_beli);?></td>
					<td><?php echo $sql->bonus;?></td>
					<td><?php echo $sql->status;?></td>
					<td><?php echo $sql->status_bayar;?></td>
					<td>
						<a href="javascript:void(0);" class="btn btn-info btn-xs" data-toggle="modal" data-target=".gambar<?php echo $sql->id_beli;?>">
							<i class="fa fa-eye"></i> Lihat Gambar
						</a>
					</td>
					<td>
						<a class="btn btn-xs btn-info" data-toggle="modal" data-target=".status<?php echo $sql->id_beli;?>"><i class="fa fa-edit" title="Ubah Status"></i> Ubah Status</a>
					</td>
				</tr>
				<?php
					}
				?>
				</tbody>
			</table>
		</div>
	</div>
</section>

<?php 
	foreach($main['sql'] as $sql){
?>
<div class="modal fade gambar<?php echo $sql->id_beli;?>" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
	<div class="modal-dialog modal-md" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			</div>
			<div class="modal-body">
				<?php if ($sql->foto_bukti_pembayaran == NULL) echo "Gambar belum di upload !";
					else {
				?>
				<img src="http://localhost/jsit_koperasi/upload/bukti_bayar/<?php echo $sql->foto_bukti_pembayaran;?>" width="100%">
				<?php
					}
				?>
			</div>
		</div>
	</div>
</div>
<?php
	}
?>

<?php 
	foreach($main['sql'] as $sql){
?>
<div class="modal fade status<?php echo $sql->id_beli;?>" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
	<div class="modal-dialog modal-md" role="document">
	<?php echo form_open_multipart('pesanan/update/'); ?>
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<div class="form-group row">
					<label class="col-md-3 col-form-label">Nama Pemesan</label>
					<div class="col-md-9">
						<input type="text" name="nama_pemesan" id="nama_pemesan" class="form-control" placeholder="Nama Pemesan" value="<?php echo $sql->nama_user;?>" readonly>
						<input type="hidden" name="id_beli" id="id_beli" class="form-control" placeholder="ID Beli" value="<?php echo $sql->id_beli;?>">
					</div>
				</div>
				<div class="form-group row">
					<label class="col-md-3 col-form-label">Status Pesanan</label>
					<div class="col-md-9">
						<select name="status" id="status" class="form-control">
							<option value="Baru" <?php if($sql->status=="Baru") echo "selected";?>>Baru</option>
							<option value="Buat" <?php if($sql->status=="Buat") echo "selected";?>>Buat</option>
							<option value="Packing" <?php if($sql->status=="Packing") echo "selected";?>>Packing</option>
							<option value="Kirim" <?php if($sql->status=="Kirim") echo "selected";?>>Kirim</option>
							<option value="Terima" <?php if($sql->status=="Terima") echo "selected";?>>Terima</option>
							<option value="Batal" <?php if($sql->status=="Batal") echo "selected";?>>Batal</option>
						</select>
						<input type="hidden" name="status2" id="status2" value="<?php echo $sql->status;?>">
					</div>
				</div>
				<div class="form-group row">
					<label class="col-md-3 col-form-label">Status Pembayaran</label>
					<div class="col-md-9">
						<select name="status_bayar" id="status_bayar" class="form-control">
							<option value="Sudah" <?php if($sql->status_bayar=="Sudah") echo "selected";?>>Sudah</option>
							<option value="Belum" <?php if($sql->status_bayar=="Belum") echo "selected";?>>Belum</option>
						</select>
						<input type="hidden" name="status_bayar2" id="status_bayar2" value="<?php echo $sql->status_bayar;?>">
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
				<button type="submit" type="submit" class="btn btn-primary">Update</button>
			</div>
		</div>
	<?php echo form_close(); ?>
	</div>
</div>
<?php
	}
?>

<!-- <div class="modal fade" id="lihat_gambar" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
	<div class="modal-dialog modal-md" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			</div>
			<div class="modal-body">
				<img id="foto_bukti" width="100%">
				<p id="foto_bukti_kosong" style="display: none;">Gambar belum di upload !</p>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="edit_status" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-md" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
						<div class="form-group row">
								<label class="col-md-3 col-form-label">Nama Pemesan</label>
								<div class="col-md-9">
									<input type="text" name="nama_pemesan" id="nama_pemesan" class="form-control" placeholder="Nama Pemesan" readonly>
									<input type="hidden" name="id_beli" id="id_beli" class="form-control" placeholder="ID Beli">
								</div>
						</div>
						<div class="form-group row">
								<label class="col-md-3 col-form-label">Status Pesanan</label>
								<div class="col-md-9">
									<select name="status" id="status" class="form-control">
										<option value="Baru">Baru</option>
										<option value="Buat">Buat</option>
										<option value="Packing">Packing</option>
										<option value="Kirim">Kirim</option>
										<option value="Terima">Terima</option>
										<option value="Batal">Batal</option>
									</select>
									<input type="hidden" name="status2" id="status2">
								</div>
						</div>
						<div class="form-group row">
								<label class="col-md-3 col-form-label">Status Pembayaran</label>
								<div class="col-md-9">
									<select name="status_bayar" id="status_bayar" class="form-control">
										<option value="Sudah">Sudah</option>
										<option value="Belum">Belum</option>
									</select>
									<input type="hidden" name="status_bayar2" id="status_bayar2">
								</div>
						</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
				<button type="button" type="submit" id="btn_update" class="btn btn-primary">Update</button>
			</div>
		</div>
	</div>
</div> -->

<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery-3.2.1.js"></script>
<script src="<?php echo base_url();?>assets/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url();?>assets/plugins/datatables/dataTables.bootstrap.min.js"></script>
<script type="text/javascript">
$(document).ready(function(){
	show_pesanan();

	$("#myData").dataTable({
		dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excelHtml5', 'pdf', 'print'
        ],
        "_fnGetHeaders": function() {
            var dt = this.s.dt;
            var thRows = dt.nTHead.rows;
            var numRows = thRows.length;
            var matrix = [];
     
            // Iterate over each row of the header and add information to matrix.
            for ( var rowIdx = 0;  rowIdx < numRows;  rowIdx++ ) {
                var $row = $(thRows[rowIdx]);
     
                // Iterate over actual columns specified in this row.
                var $ths = $row.children("th");
                for ( var colIdx = 0;  colIdx < $ths.length;  colIdx++ )
                {
                    var $th = $($ths.get(colIdx));
                    var colspan = $th.attr("colspan") || 1;
                    var rowspan = $th.attr("rowspan") || 1;
                    var colCount = 0;
                  
                    // ----- add this cell's title to the matrix
                    if (matrix[rowIdx] === undefined) {
                        matrix[rowIdx] = [];  // create array for this row
                    }
                    // find 1st empty cell
                    for ( var j = 0;  j < (matrix[rowIdx]).length;  j++, colCount++ ) {
                        if ( matrix[rowIdx][j] === "PLACEHOLDER" ) {
                            break;
                        }
                    }
                    var myColCount = colCount;
                    matrix[rowIdx][colCount++] = $th.text();
                 
                    // ----- If title cell has colspan, add empty titles for extra cell width.
                    for ( var j = 1;  j < colspan;  j++ ) {
                        matrix[rowIdx][colCount++] = "";
                    }
                  
                    // ----- If title cell has rowspan, add empty titles for extra cell height.
                    for ( var i = 1;  i < rowspan;  i++ ) {
                        var thisRow = rowIdx+i;
                        if ( matrix[thisRow] === undefined ) {
                            matrix[thisRow] = [];
                        }
                        // First add placeholder text for any previous columns.                
                        for ( var j = (matrix[thisRow]).length;  j < myColCount;  j++ ) {
                            matrix[thisRow][j] = "PLACEHOLDER";
                        }
                        for ( var j = 0;  j < colspan;  j++ ) {  // and empty for my columns
                            matrix[thisRow][myColCount+j] = "";
                        }
                    }
                }
            }
            
            return matrix;
        },
	});
	if ( oConfig.bHeader )
        {
/* ----- BEGIN changed Code ----- */           
            var headerMatrix = this._fnGetHeaders();
            for ( var rowIdx = 0;  rowIdx < headerMatrix.length;  rowIdx++ ) {
                aRow = [];
                for ( var colIdx = 0;  colIdx < (headerMatrix[rowIdx]).length;  colIdx++ ) {
                    sLoopData = headerMatrix[rowIdx][colIdx].replace(/\n/g," ").replace( /<.*?>/g, "" ).replace(/^\s+|\s+$/g,"");
                    sLoopData = this._fnHtmlDecode( sLoopData );
 
                    aRow.push( this._fnBoundData( sLoopData, oConfig.sFieldBoundary, regex ) );
                }
                aData.push( aRow.join(oConfig.sFieldSeperator) );
            }
/* ----- END changed Code ----- */
        }

	function show_pesanan(){
		$.ajax({
			url   : '<?php echo site_url('pesanan/data_pesanan')?>',
			async : false,
			dataType : 'json',
			success : function(data){
				var html = '';
				var i;
				for(i=0; i<data.length; i++){
					var id_beli = data[i].id_beli;
					var id_user_beli = data[i].id_user_beli;
					var j = i+1;
					html += 
					'<tr>'+
						'<td>'+j+'</td>'+
						'<td>'+data[i].nama_user+'</td>'+
						'<td>'+data[i].judul_produk+'</td>'+
						'<td>'+data[i].total_beli+'</td>'+
						'<td>'+data[i].status+'</td>'+
						'<td>'+data[i].status_bayar+'</td>'+
						'<td>'+
							'<a href="javascript:void(0);" class="btn btn-info btn-xs item_lihat" data-id_beli="'+data[i].id_beli+'" data-foto_bukti="'+data[i].foto_bukti_pembayaran+'">'+
								'<i class="fa fa-eye"></i> Lihat Gambar'+
							'</a>'+
						'</td>'+
						'<td>'+
							'<a class="btn btn-xs btn-info item_status" data-id_beli="'+data[i].id_beli+'" data-status="'+data[i].status+'" data-status_bayar="'+data[i].status_bayar+'" data-nama_pemesan="'+data[i].nama_user+'"><i class="fa fa-edit" title="Ubah Status"></i> Ubah Status</a>'+
						'</td>'+
					'</tr>';
				}
				$('#show_pesanan').html(html);
			}

		});
	}

	$('#show_pesanan').on('click','.item_lihat',function(){
			var id_beli = $(this).data('id_beli');
			var foto_bukti = $(this).data('foto_bukti');
			
			$('#lihat_gambar').modal('show');
			document.getElementById("foto_bukti").src = 'http://localhost/jsit_koperasi/upload/bukti_bayar/'+foto_bukti;
	});

	$('.item_lihat').on('click',function(){
			var id_beli = $(this).data('id_beli');
			var foto_bukti = $(this).data('foto_bukti');
			
			$('#lihat_gambar').modal('show');
			if(foto_bukti == ""){
				document.getElementById("foto_bukti").style.display = 'none';
				document.getElementById("foto_bukti_kosong").style.display = 'block';
			}else{
				document.getElementById("foto_bukti_kosong").style.display = 'none';
				document.getElementById("foto_bukti").src = 'http://localhost/jsit_koperasi/upload/bukti_bayar/'+foto_bukti;
			}
	});

	$('#show_pesanan').on('click','.item_status',function(){
			var id_beli = $(this).data('id_beli');
			var status = $(this).data('status');
			var status_bayar = $(this).data('status_bayar');
			var nama_pemesan = $(this).data('nama_pemesan');
			
			$('#edit_status').modal('show');
			$('[name="id_beli"]').val(id_beli);
			$('[name="nama_pemesan"]').val(nama_pemesan);
			$('[name="status"]').val(status);
			$('[name="status2"]').val(status);
			$('[name="status_bayar"]').val(status_bayar);
			$('[name="status_bayar2"]').val(status_bayar);
	});

	$('.item_status').on('click',function(){
			var id_beli = $(this).data('id_beli');
			var status = $(this).data('status');
			var status_bayar = $(this).data('status_bayar');
			var nama_pemesan = $(this).data('nama_pemesan');
			
			$('#edit_status').modal('show');
			$('[name="id_beli"]').val(id_beli);
			$('[name="nama_pemesan"]').val(nama_pemesan);
			$('[name="status"]').val(status);
			$('[name="status2"]').val(status);
			$('[name="status_bayar"]').val(status_bayar);
			$('[name="status_bayar2"]').val(status_bayar);
	});

	$('#btn_update').on('click',function(){
		var id_beli = $('#id_beli').val();
		var status = $('#status').val();
		var status2 = $('#status2').val();
		var status_bayar = $('#status_bayar').val();
		var status_bayar2 = $('#status_bayar2').val();
		if(status_bayar2!=status_bayar && status2!=status){
			$.ajax({
				type : "POST",
				url  : "<?php echo site_url('pesanan/update_pesanan')?>",
				dataType : "JSON",
				data : {id_beli:id_beli , status:status},
				success: function(data){
					$('[name="status"]').val("");
					$('[name="status2"]').val("");
					update_bayar();
					window.location = "<?php echo site_url('pesanan');?>";
				}
			});
			return false;
		}else if(status2!=status){
			$.ajax({
				type : "POST",
				url  : "<?php echo site_url('pesanan/update_pesanan')?>",
				dataType : "JSON",
				data : {id_beli:id_beli , status:status},
				success: function(data){
					$('[name="nama_pemesan"]').val("");
					$('[name="id_beli"]').val("");
					$('[name="status"]').val("");
					$('[name="status2"]').val("");
					$('[name="status_bayar"]').val("");
					$('[name="status_bayar2"]').val("");
					$('#edit_status').modal('hide');
					show_pesanan();
					window.location = "<?php echo site_url('pesanan');?>";
				}
			});
			return false;
		}else if(status_bayar2!=status_bayar){
			$.ajax({
				type : "POST",
				url  : "<?php echo site_url('pesanan/update_pesanan_bayar')?>",
				dataType : "JSON",
				data : {id_beli:id_beli, status_bayar:status_bayar},
				success: function(data){
					$('[name="nama_pemesan"]').val("");
					$('[name="id_beli"]').val("");
					$('[name="status"]').val("");
					$('[name="status2"]').val("");
					$('[name="status_bayar"]').val("");
					$('[name="status_bayar2"]').val("");
					$('#edit_status').modal('hide');
					show_pesanan();
					window.location = "<?php echo site_url('pesanan');?>";
				}
			});
			return false;
		}else{
			$('[name="nama_pemesan"]').val("");
			$('[name="id_beli"]').val("");
			$('[name="status"]').val("");
			$('[name="status2"]').val("");
			$('[name="status_bayar"]').val("");
			$('[name="status_bayar2"]').val("");
			$('#edit_status').modal('hide');
			show_pesanan();
			window.location = "<?php echo site_url('pesanan');?>";
		};
	});

	function update_bayar(){
		var id_beli = $('#id_beli').val();
		var status_bayar = $('#status_bayar').val();
		$.ajax({
			type : "POST",
			url  : "<?php echo site_url('pesanan/update_pesanan_bayar')?>",
			dataType : "JSON",
			data : {id_beli:id_beli, status_bayar:status_bayar},
			success: function(data){
				$('[name="nama_pemesan"]').val("");
				$('[name="id_beli"]').val("");
				$('[name="status_bayar"]').val("");
				$('[name="status_bayar2"]').val("");
				$('#edit_status').modal('hide');
				show_pesanan();
				window.location = "<?php echo site_url('pesanan');?>";
			}
		});
		return false;
	}
});
</script>
