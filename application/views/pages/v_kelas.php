<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
      Data Kelas
    </h1>
    <ol class="breadcrumb">
      <li><a href="<?php echo site_url("home")?>"><i class="fa fa-home"></i> Home</a></li>
      <li>Produk</li>
      <li class="active">Kelas</li>
    </ol><br>
    <?php echo $this->session->flashdata('notif')?>
</section>

  <!-- Main content -->
<section class="content">
    <!-- Info boxes -->
    <div class="box">
        <div class="box-header">
            <a href="<?php echo site_url('core_produk/form_kelas')?>" class="btn btn-hajj"><i class="fa fa-plus"></i> Tambah </a>
        </div>
        <div class="box-body">
            <table id="example1" class="table table-striped table-bordered">
                <thead>
                    <tr>
                        <th>No.</th>
                        <th>Nama Kelas</th>
                        <th width="91">Action</th>
                    </tr>
                </thead>

                <tbody>
                <?php
                    $no=0;
                    foreach ($main['sql']->result() as $obj)
                    {
                        $id = $obj->id_kelas;
                        $no++;
                ?>
                    <tr>
                        <td><?php echo $no;?></td>
                        <td><?php echo $obj->nama_kelas;?></td>
                        <td>
                            <a class="btn btn-xs btn-info" href="<?php echo site_url();?>core_produk/form_edit_kelas/<?php echo $id;?>"><i class='fa fa-edit'></i></a>
                            <a  class="btn btn-xs btn-danger" href="javascript:if(confirm('Apakah Anda yakin ?')){document.location='<?php echo site_url();?>/core_produk/delete_kelas/<?php echo $id;?>';}"><i class='fa fa-trash'></i></a>
                        </td>
                    </tr>
                <?php
                    }
                ?>
                </tbody>
            </table>
        </div>
    </div>
</section>
