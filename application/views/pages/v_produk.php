<!-- Content Header (Page header) -->
<section class="content-header">
	<h1>
		Config Produk
	</h1>
	<ol class="breadcrumb">
		<li><a href="<?php echo site_url("home") ?>"><i class="fa fa-home"></i> Home</a></li>
		<li>Config</li>
		<li class="active">Produk</li>
	</ol><br>
	<?php echo $this->session->flashdata('notif') ?>
</section>

<?php
function convert_to_rupiah($angka)
	{
		return 'Rp. '.strrev(implode('.',str_split(strrev(strval($angka)),3)));
	}
?>
<!-- Main content -->
<section class="content">
	<!-- Info boxes -->
	<div class="box">
		<div class="box-header">
			<?php
			if ($this->session->userdata('role') == '1') {
			?>
				<a href="<?php echo site_url('produk/form') ?>" class="btn btn-hajj"><i class="fa fa-plus"></i> Tambah </a>
			<?php
			}
			?>
		</div>
		<div class="box-body">
			<table id="example1" class="table table-striped table-bordered">
				<thead>
					<tr>
						<th>No.</th>
						<th>ISBN</th>
						<th>Judul</th>
						<th>Kelas</th>
						<th>Kategori</th>
						<th>Harga</th>
						<th>Stok</th>
						<th>Foto</th>
						<th width="91">Action</th>
					</tr>
				</thead>

				<tbody>
					<?php
					$no = 0;
					foreach ($main['sql']->result() as $obj) 
					{
						$id = $obj->id_produk;
						$no++;
					?>
						<tr>
							<td><?php echo $no; ?></td>
							<td><?php echo $obj->isbn_produk; ?></td>
							<td><?php echo $obj->judul_produk; ?></td>
							<td>
								<?php
								foreach ($main['kelas']->result() as $kelas) {
									if ($obj->id_kelas == $kelas->id_kelas) {
										echo $kelas->nama_kelas;
									}
								}
								?>
							</td>
							<td>
								<?php
								foreach ($main['kategori']->result() as $kategori) {
									if ($obj->id_kategori == $kategori->id_kategori) {
										echo $kategori->nama_kategori;
									}
								}
								?>
							</td>
							<td><?php echo convert_to_rupiah($obj->harga_produk); ?></td>
							<td><?php echo $obj->stok; ?></td>
							<td>
								<a href="#" class="btn btn-xs" data-toggle="modal" data-target=".gambar<?php echo $id; ?>">
									<i class='fa fa-eye'></i> Lihat Gambar
								</a>
							</td>
							<td>
								<?php
								if ($this->session->userdata('role') == '1') {
								?>
									<a class="btn btn-xs btn-info" href="<?php echo site_url(); ?>produk/form_edit/<?php echo $id; ?>"><i class='fa fa-edit'></i></a>
									<a class="btn btn-xs btn-danger" href="javascript:if(confirm('Apakah Anda yakin ?')){document.location='<?php echo site_url(); ?>/produk/delete/<?php echo $id; ?>';}"><i class='fa fa-trash'></i></a>
								<?php
								} else if ($this->session->userdata('role') == '3') {
								?>
									<a class="btn btn-xs btn-info" href="#" data-toggle="modal" data-target=".modal_stok<?php echo $id; ?>"><i class='fa fa-edit'></i></a>
								<?php
								}
								?>
							</td>
						</tr>
					<?php
					}
					?>
				</tbody>
			</table>
		</div>
	</div>
</section>

<!-- Modal -->
<?php
foreach ($main['sql']->result() as $obj) {
	$id = $obj->id_produk;
?>
	<div class="modal fade gambar<?php echo $id; ?>" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
		<div class="modal-dialog modal-md" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				</div>
				<div class="modal-body">
					<img src="<?php if ($obj->foto_produk == NULL) echo site_url('assets/images/dummy.png');
										else echo site_url("upload/produk/$obj->foto_produk"); ?>" width="100%">
				</div>
			</div>
		</div>
	</div>
<?php
}
?>

<?php
if ($this->session->userdata('role') == '3') {
foreach ($main['sql']->result() as $obj) 
{
	$id = $obj->id_produk;
?>
	<?php echo form_open_multipart('produk/create/'); ?>
	<input type="hidden" name="op" value="edit_stok">
	<input type="hidden" name="id" value="<?php echo $id; ?>">
	<div class="modal fade modal_stok<?php echo $id; ?>" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
		<div class="modal-dialog modal-md" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				</div>
				<div class="modal-body">
					<div class="form-group">
						<label for="inputName" class="col-sm-2 control-label">Stok</label>
						<div class="col-sm-10">
							<input type="text" name="stok" value="<?php echo $obj->stok; ?>" class="form-control" id="inputName" placeholder="Stok" required>
						</div>
					</div>
					<br>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Kembali</button>
					<button type="submit" class="btn btn-primary">
						Simpan
					</button>
				</div>
			</div>
		</div>
	</div>
	<?php echo form_close(); ?>
<?php
}
}
?>
