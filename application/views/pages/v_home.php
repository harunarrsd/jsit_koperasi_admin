<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
      Dashboard
    </h1>
    <ol class="breadcrumb">
      <li><a href="<?php echo site_url("home")?>"><i class="fa fa-home"></i> Home</a></li>
      <li class="active">Dashboard</li>
    </ol>
</section>

  <!-- Main content -->
    <section class="content">
        <!-- Small boxes (Stat box) -->
      <div class="row">
        <div class="col-lg-4 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-aqua">
            <div class="inner">
              <h3><?php echo $main['member'];?></h3>
              <p>Member JSIT</p>
            </div>
            <div class="icon">
              <!-- <i class="ion ion-person"></i> -->
              <i class="fa fa-users "></i>
            </div>
            <a href="<?php echo site_url('.');?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <!-- <div class="col-lg-3 col-xs-6">
          <div class="small-box bg-green">
            <div class="inner">
				<h3>0</h3>
              <p>Member Non JSIT</p>
            </div>
            <div class="icon">
              <i class="fa fa-book"></i>
            </div>
            <a href="<?php echo site_url('.');?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div> -->
        <!-- ./col -->
        <div class="col-lg-4 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-yellow">
            <div class="inner">
				<h3><?php echo $main['pesanan'];?></h3>
              <p>Pesanan</p>
            </div>
            <div class="icon">
              <!-- <i class="ion ion-document"></i> -->
              <i class="fa fa-file"></i>
            </div>
            <a href="<?php echo site_url('.');?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-4 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-red">
            <div class="inner">
				<h3><?php echo $main['komplain'];?></h3>
              <p>Komplain</p>
            </div>
            <div class="icon">
              <!-- <i class="ion ion-pie-graph"></i> -->
              <i class="fa fa-sticky-note"></i>
            </div>
            <a href="<?php echo site_url('.');?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
      </div>
    </section>
