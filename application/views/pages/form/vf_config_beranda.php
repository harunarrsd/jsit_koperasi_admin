<?php
$id = "";
$caption_satu = "";
$caption_dua = "";
$text_button = "";
$background = "";
if ($main['op'] == "edit") {
	foreach ($main['sql']->result() as $sql) {
		$op = "edit";
		$id = $sql->id_cb;
		$caption_satu = $sql->caption_satu;
		$caption_dua = $sql->caption_dua;
		$text_button = $sql->text_button;
		$background = $sql->background;
	}
}
?>
<!-- Content Header (Page header) -->
<section class="content-header">
	<h1>
		Form Config Beranda
	</h1>
	<ol class="breadcrumb">
		<li><a href="<?php echo site_url("home") ?>"><i class="fa fa-home"></i> Home</a></li>
		<li>Config</li>
		<li><a href="<?php echo site_url("config_beranda") ?>">Beranda</a></li>
		<li class="active">Form Config Beranda</li>
	</ol><br>
	<?php echo $this->session->flashdata('notif') ?>
</section>

<!-- Main content -->
<section class="content">
	<!-- Info boxes -->
	<!-- Horizontal Form -->
	<div class="box box-info">
		<div class="box-header with-border text-center">
			<h4><b>Slider Beranda</b></h4>
		</div>
		<!-- /.box-header -->
		<!-- form start -->
		<?php echo form_open_multipart('config_beranda/create/'); ?>
		<input type="hidden" name="op" value="<?php echo $main['op']; ?>">
		<input type="hidden" name="id" value="<?php echo $id; ?>">
		<div class="box-body form-horizontal">
			<div class="form-group">
				<label for="inputName" class="col-sm-2 control-label">Caption Satu</label>
				<div class="col-sm-10">
					<input type="text" name="caption_satu" value="<?php echo $caption_satu; ?>" class="form-control" id="inputName" placeholder="Caption Satu" required>
				</div>
			</div>
			<div class="form-group">
				<label for="inputName" class="col-sm-2 control-label">Caption Dua</label>
				<div class="col-sm-10">
					<input type="text" name="caption_dua" value="<?php echo $caption_dua; ?>" class="form-control" id="inputName" placeholder="Caption Dua" required>
				</div>
			</div>
			<div class="form-group">
				<label for="inputName" class="col-sm-2 control-label">Text Button</label>
				<div class="col-sm-10">
					<input type="text" name="text_button" value="<?php echo $text_button; ?>" class="form-control" id="inputName" placeholder="Text Button" required>
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-2 control-label">Background</label>
				<div class="col-sm-10">
					<input type="file" name="gambar" value="<?php echo $background; ?>" class="form-control" <?php if ($main['op'] == 'tambah') echo 'required' ?>>
				</div>
			</div>
			<div class="form-group">
				<div class="col-sm-2 control-label"></div>
				<div class="col-sm-10">
					<a href="<?php echo site_url('config_beranda') ?>" class="btn btn-danger" style="color:white;">Kembali</a>
					<button type="submit" class="btn btn-hajj">Submit</button>
				</div>
			</div>
		</div>
		<!-- /.box-body -->
		</form>
	</div>
	<!-- /.box -->
</section>
<!-- /.content -->
