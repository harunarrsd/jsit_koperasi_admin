<?php
  $id= "";
  $isbn_produk = "";
  $judul_produk = "";
  $id_kelas = "";
  $harga_produk = "";
  $stok = "";
  $id_kategori = "";
  $spesifikasi = "";
  $foto_produk = "";
  if ($main['op']=="edit") {
    foreach ($main['sql']->result() as $sql) {
      $op = "edit";
      $id = $sql->id_produk;
      $isbn_produk = $sql->isbn_produk;
      $judul_produk = $sql->judul_produk;
      $id_kelas = $sql->id_kelas;
      $harga_produk = $sql->harga_produk;
      $stok = $sql->stok;
      $id_kategori = $sql->id_kategori;
      $spesifikasi = $sql->spesifikasi;
      $foto_produk = $sql->foto_produk;
    }
  }
?>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
      Form Produk
    </h1>
    <ol class="breadcrumb">
      <li><a href="<?php echo site_url("home")?>"><i class="fa fa-home"></i> Home</a></li>
      <li>Config</li>
      <li><a href="<?php echo site_url("produk")?>">Produk</a></li>
      <li class="active">Form Produk</li>
    </ol><br>
    <?php echo $this->session->flashdata('notif')?>
</section>

  <!-- Main content -->
<section class="content">
    <!-- Info boxes -->
    <!-- Horizontal Form -->
    <div class="box box-info">
        <div class="box-header with-border text-center">

        </div>
        <!-- /.box-header -->
        <!-- form start -->
        <?php echo form_open_multipart('produk/create/');?>
          <input type="hidden" name="op" value="<?php echo $main['op'];?>">
		  <input type="hidden" name="id" value="<?php echo $id;?>">
          <div class="box-body form-horizontal">
            <div class="form-group">
                <label for="inputName" class="col-sm-2 control-label">ISBN Produk</label>
                <div class="col-sm-10">
                    <input type="text" name="isbn_produk" value="<?php echo $isbn_produk;?>" class="form-control" id="inputName" placeholder="ISBN Produk" required>
                </div>
			</div>
			<div class="form-group">
                <label for="inputName" class="col-sm-2 control-label">Judul Produk</label>
                <div class="col-sm-10">
                    <input type="text" name="judul_produk" value="<?php echo $judul_produk;?>" class="form-control" id="inputName" placeholder="Judul Produk" required>
                </div>
			</div>
			<div class="form-group">
                <label for="inputName" class="col-sm-2 control-label">Kelas</label>
                <div class="col-sm-10">
                    <select name="id_kelas" class="form-control">
						<option value="">Pilih</option>
						<?php 
							foreach($main['kelas']->result() as $kelas){
								?>
								<option value="<?php echo $kelas->id_kelas;?>" <?php if($id_kelas==$kelas->id_kelas)echo 'selected';?>>
									<?php echo $kelas->nama_kelas;?>
								</option>
								<?php
							}
						?>
					</select>
                </div>
			</div>
			<div class="form-group">
                <label for="inputName" class="col-sm-2 control-label">Kategori</label>
                <div class="col-sm-10">
                    <select name="id_kategori" class="form-control">
						<option value="">Pilih</option>
						<?php 
							foreach($main['kategori']->result() as $kategori){
								?>
								<option value="<?php echo $kategori->id_kategori;?>" <?php if($id_kategori==$kategori->id_kategori)echo 'selected';?>>
									<?php echo $kategori->nama_kategori;?>
								</option>
								<?php
							}
						?>
					</select>
                </div>
			</div>
			<div class="form-group">
                <label for="inputName" class="col-sm-2 control-label">Spesifikasi Produk</label>
                <div class="col-sm-10">
					<textarea id="editor1" name="spesifikasi" required><?php echo $spesifikasi;?></textarea>
                </div>
			</div>
			<div class="form-group">
                <label for="inputName" class="col-sm-2 control-label">Harga Produk</label>
                <div class="col-sm-10">
                    <input type="text" name="harga_produk" value="<?php echo $harga_produk;?>" class="form-control" id="inputName" placeholder="Harga Produk" required>
                </div>
			</div>
			<!-- <div class="form-group">
                <label for="inputName" class="col-sm-2 control-label">Stok</label>
                <div class="col-sm-10">
                    <input type="text" name="stok" value="<?php echo $stok;?>" class="form-control" id="inputName" placeholder="Stok" required>
                </div>
			</div> -->
            <div class="form-group">
                <label class="col-sm-2 control-label">Foto Produk</label>
                <div class="col-sm-10">
                    <input type="file" name="gambar" value="<?php echo $foto_produk;?>" class="form-control" <?php if($main['op']=='tambah') echo 'required'?>>
                </div>
			</div>
            <div class="form-group">
              <div class="col-sm-2 control-label"></div>
              <div class="col-sm-10">
				<a href="<?php echo site_url('produk')?>"class="btn btn-danger" style="color:white;">Kembali</a>
				<button type="submit" class="btn btn-hajj">Submit</button>
			</div>
            </div>
          </div>
          <!-- /.box-body -->
        </form>
    </div>
          <!-- /.box -->
</section>
  <!-- /.content -->
