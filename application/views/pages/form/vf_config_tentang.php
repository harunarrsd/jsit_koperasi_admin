<?php
  $id= "";
  $judul_halaman = "";
  $cover_halaman = "";
  $judul_tentang = "";
  $isi_tentang = "";
  $nama_quotes = "";
  $isi_quotes = "";
  $foto_perusahaan = "";
  if ($main['op']=="edit") {
    foreach ($main['sql']->result() as $sql) {
      $op = "edit";
      $id = $sql->id_ct;
      $judul_halaman = $sql->judul_halaman;
      $cover_halaman = $sql->cover_halaman;
      $judul_tentang = $sql->judul_tentang;
      $isi_tentang = $sql->isi_tentang;
      $nama_quotes = $sql->nama_quotes;
      $isi_quotes = $sql->isi_quotes;
      $foto_perusahaan = $sql->foto_perusahaan;
    }
  }
?>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
      Form Config Tentang
    </h1>
    <ol class="breadcrumb">
      <li><a href="<?php echo site_url("home")?>"><i class="fa fa-home"></i> Home</a></li>
      <li>Config</li>
      <li><a href="<?php echo site_url("config_tentang")?>">Tentang</a></li>
      <li class="active">Form Config Tentang</li>
    </ol><br>
    <?php echo $this->session->flashdata('notif')?>
</section>

  <!-- Main content -->
<section class="content">
    <!-- Info boxes -->
    <!-- Horizontal Form -->
    <div class="box box-info">
        <div class="box-header with-border text-center">
			<h4><b>Header Tentang</b></h4>
        </div>
        <!-- /.box-header -->
        <!-- form start -->
        <?php echo form_open_multipart('config_tentang/create/');?>
          <input type="hidden" name="op" value="<?php echo $main['op'];?>">
		  <input type="hidden" name="id" value="<?php echo $id;?>">
          <div class="box-body form-horizontal">
            <div class="form-group">
                <label for="inputName" class="col-sm-2 control-label">Judul Halaman</label>
                <div class="col-sm-10">
                    <input type="text" name="judul_halaman" value="<?php echo $judul_halaman;?>" class="form-control" id="inputName" placeholder="Judul Halaman" required>
                </div>
			</div>
            <div class="form-group">
                <label class="col-sm-2 control-label">Cover Halaman</label>
                <div class="col-sm-10">
                    <input type="file" name="gambar" value="<?php echo $cover_halaman;?>" class="form-control" <?php if($main['op']=='tambah') echo 'required'?>>
                </div>
			</div>
		<div class="box-header with-border text-center">
			<h4><b>Body Tentang</b></h4>
		</div><br>
			<div class="form-group">
                <label for="inputName" class="col-sm-2 control-label">Judul Tentang</label>
                <div class="col-sm-10">
                    <input type="text" name="judul_tentang" value="<?php echo $judul_tentang;?>" class="form-control" id="inputName" placeholder="Judul Tentang" required>
                </div>
			</div>
			<div class="form-group">
                <label for="inputName" class="col-sm-2 control-label">Isi Tentang</label>
                <div class="col-sm-10">
					<textarea id="editor1" name="isi_tentang" required><?php echo $isi_tentang;?></textarea>
                </div>
			</div>
			<div class="form-group">
                <label for="inputName" class="col-sm-2 control-label">Nama Quotes</label>
                <div class="col-sm-10">
                    <input type="text" name="nama_quotes" value="<?php echo $nama_quotes;?>" class="form-control" id="inputName" placeholder="Nama Quotes" required>
                </div>
			</div>
			<div class="form-group">
                <label for="inputName" class="col-sm-2 control-label">Isi Quotes</label>
                <div class="col-sm-10">
					<textarea id="editor2" name="isi_quotes" required><?php echo $isi_quotes;?></textarea>
                </div>
			</div>
            <div class="form-group">
                <label class="col-sm-2 control-label">Foto Perusahaan</label>
                <div class="col-sm-10">
                    <input type="file" name="gambar2" value="<?php echo $foto_perusahaan;?>" class="form-control" <?php if($main['op']=='tambah') echo 'required'?>>
                </div>
			</div>
            <div class="form-group">
              <div class="col-sm-2 control-label"></div>
              <div class="col-sm-10">
				<a href="<?php echo site_url('config_tentang')?>"class="btn btn-danger" style="color:white;">Kembali</a>
				<button type="submit" class="btn btn-hajj">Submit</button>
			</div>
            </div>
          </div>
          <!-- /.box-body -->
        </form>
    </div>
          <!-- /.box -->
</section>
  <!-- /.content -->
