<?php
  $id= "";
  $nama_kelas = "";
  if ($main['op']=="edit") {
    foreach ($main['sql']->result() as $sql) {
      $op = "edit";
      $id = $sql->id_kelas;
      $nama_kelas = $sql->nama_kelas;
    }
  }
?>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
      Form Kelas
    </h1>
    <ol class="breadcrumb">
      <li><a href="<?php echo site_url("home")?>"><i class="fa fa-home"></i> Home</a></li>
      <li><a href="<?php echo site_url("core_produk/kelas")?>">Produk</a></li>
      <li class="active">Form Kelas</li>
    </ol><br>
    <?php echo $this->session->flashdata('notif')?>
</section>

  <!-- Main content -->
<section class="content">
    <!-- Info boxes -->
    <!-- Horizontal Form -->
    <div class="box box-info">
        <div class="box-header with-border text-center">

        </div>
        <!-- /.box-header -->
        <!-- form start -->
        <?php echo form_open_multipart('core_produk/create_kelas/');?>
          <input type="hidden" name="op" value="<?php echo $main['op'];?>">
		  		<input type="hidden" name="id" value="<?php echo $id;?>">
          <div class="box-body form-horizontal">
            <div class="form-group">
                <label for="inputName" class="col-sm-2 control-label">Nama Kelas</label>
                <div class="col-sm-10">
                    <input type="text" name="nama_kelas" value="<?php echo $nama_kelas;?>" class="form-control" id="inputName" placeholder="Nama Kelas" required>
                </div>
						</div>
            <div class="form-group">
              <div class="col-sm-2 control-label"></div>
              <div class="col-sm-10">
								<a href="<?php echo site_url('core_produk/kelas')?>"class="btn btn-danger" style="color:white;">Kembali</a>
								<button type="submit" class="btn btn-hajj">Submit</button>
							</div>
            </div>
          </div>
          <!-- /.box-body -->
        </form>
    </div>
          <!-- /.box -->
</section>
  <!-- /.content -->
