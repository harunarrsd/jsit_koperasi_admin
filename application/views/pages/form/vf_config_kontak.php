<?php
$id = "";
$judul_halaman = "";
$cover_halaman = "";
$link_maps = "";
if ($main['op'] == "edit") {
	foreach ($main['sql']->result() as $sql) {
		$op = "edit";
		$id = $sql->id_ck;
		$judul_halaman = $sql->judul_halaman;
		$cover_halaman = $sql->cover_halaman;
		$link_maps = $sql->link_maps;
	}
}
?>
<!-- Content Header (Page header) -->
<section class="content-header">
	<h1>
		Form Config Kontak
	</h1>
	<ol class="breadcrumb">
		<li><a href="<?php echo site_url("home") ?>"><i class="fa fa-home"></i> Home</a></li>
		<li>Config</li>
		<li><a href="<?php echo site_url("config_kontak") ?>">Kontak</a></li>
		<li class="active">Form Config Kontak</li>
	</ol><br>
	<?php echo $this->session->flashdata('notif') ?>
</section>

<!-- Main content -->
<section class="content">
	<!-- Info boxes -->
	<!-- Horizontal Form -->
	<div class="box box-info">
		<div class="box-header with-border text-center">
			<h4><b>Header Kontak</b></h4>
		</div>
		<!-- /.box-header -->
		<!-- form start -->
		<?php echo form_open_multipart('config_kontak/create/'); ?>
		<input type="hidden" name="op" value="<?php echo $main['op']; ?>">
		<input type="hidden" name="id" value="<?php echo $id; ?>">
		<div class="box-body form-horizontal">
			<div class="form-group">
				<label for="inputName" class="col-sm-2 control-label">Judul Halaman</label>
				<div class="col-sm-10">
					<input type="text" name="judul_halaman" value="<?php echo $judul_halaman; ?>" class="form-control" id="inputName" placeholder="Judul Halaman" required>
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-2 control-label">Cover Halaman</label>
				<div class="col-sm-10">
					<input type="file" name="gambar" value="<?php echo $cover_halaman; ?>" class="form-control" <?php if ($main['op'] == 'tambah') echo 'required' ?>>
				</div>
			</div>
			<div class="box-header with-border text-center">
				<h4><b>Body Tentang</b></h4>
			</div><br>
			<div class="form-group">
				<label for="inputName" class="col-sm-2 control-label">Link Maps</label>
				<div class="col-sm-10">
					<textarea name="link_maps" class="form-control" placeholder="Link Maps" required><?php echo $link_maps; ?></textarea>
				</div>
			</div>
			<div class="form-group">
				<div class="col-sm-2 control-label"></div>
				<div class="col-sm-10">
					<a href="<?php echo site_url('config_kontak') ?>" class="btn btn-danger" style="color:white;">Kembali</a>
					<button type="submit" class="btn btn-hajj">Submit</button>
				</div>
			</div>
		</div>
		<!-- /.box-body -->
		</form>
	</div>
	<!-- /.box -->
</section>
<!-- /.content -->
