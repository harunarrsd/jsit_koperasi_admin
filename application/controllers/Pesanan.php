<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pesanan extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->library('upload');
		$this->load->helper('text');
		$this->load->model('m_jsit');
		if ($this->session->userdata('udhmasuk')==false) {
			redirect('.');
		}
	}

	public function index()	{
		$data['title'] = 'Koperasi JSIT';
		$data['sql'] = $this->m_jsit->read_pesanan()->result();
		$data['sidebar'] = $this->load->view('layouts/sidebar','',true);
        $data['pages'] = $this->load->view('pages/v_pesanan',array('main'=>$data),true);
		$this->load->view('master',array('main'=>$data));
	}

	function update(){
		$id_beli = $this->input->post('id_beli');
		$status = $this->input->post('status');
		$status2 = $this->input->post('status2');
		$status_bayar = $this->input->post('status_bayar');
		$status_bayar2 = $this->input->post('status_bayar2');
		if($status_bayar2!=$status_bayar && $status2!=$status){
			$this->m_jsit->update_pesanan();
			$this->m_jsit->update_pesanan_bayar();
			$this->session->set_flashdata('notif','<div class="alert alert-success alert-dismissible"><strong> Data berhasil diubah !</strong><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button></div>');
			redirect('pesanan');
		}else if($status2!=$status){
			$this->m_jsit->update_pesanan();
			$this->session->set_flashdata('notif','<div class="alert alert-success alert-dismissible"><strong> Data berhasil diubah !</strong><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button></div>');
			redirect('pesanan');
		}else if($status_bayar2!=$status_bayar){
			$this->m_jsit->update_pesanan_bayar();
			$this->session->set_flashdata('notif','<div class="alert alert-success alert-dismissible"><strong> Data berhasil diubah !</strong><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button></div>');
			redirect('pesanan');
		}else{
			redirect('pesanan');
		}
	}

	function data_pesanan(){
		$data=$this->m_jsit->read_pesanan()->result();
		echo json_encode($data);
	}

	function update_pesanan(){
		$data=$this->m_jsit->update_pesanan();
		echo json_encode($data);
	}
	function update_pesanan_bayar(){
		$data=$this->m_jsit->update_pesanan_bayar();
		echo json_encode($data);
	}
}
