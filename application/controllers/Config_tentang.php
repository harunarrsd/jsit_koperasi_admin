<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Config_tentang extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->library('upload');
		$this->load->helper('text');
		$this->load->model('m_jsit');
		if ($this->session->userdata('udhmasuk')==false) {
			redirect('.');
		}
	}

	public function index()	{
		$data['title'] = 'Koperasi JSIT';
		$data['sql'] = $this->m_jsit->read_tentang();
		$data['sidebar'] = $this->load->view('layouts/sidebar','',true);
        $data['pages'] = $this->load->view('pages/v_config_tentang',array('main'=>$data),true);
		$this->load->view('master',array('main'=>$data));
	}

	public function form() {
		$data['title'] = 'Koperasi JSIT';
		$data['op'] = 'tambah';
		$data['sidebar'] = $this->load->view('layouts/sidebar','',true);
        $data['pages'] = $this->load->view('pages/form/vf_config_tentang',array('main'=>$data),true);
		$this->load->view('master',array('main'=>$data));
	}

	public function form_edit($id) {
		$data['title'] = 'Koperasi JSIT';
		$data['op'] = 'edit';
		$data['sql'] = $this->m_jsit->edit_tentang($id);
		$data['sidebar'] = $this->load->view('layouts/sidebar','',true);
        $data['pages'] = $this->load->view('pages/form/vf_config_tentang',array('main'=>$data),true);
		$this->load->view('master',array('main'=>$data));
	}

	function create() {
		$op = $this->input->post('op');
		$id = $this->input->post('id');
		$filename = "cover"."_".date("YmdHis");
		$filename2 = date("YmdHis");
    	if ($op=="tambah") {
    		$config = array(
				'upload_path'=>'upload/config_tentang/',
				'allowed_types'=>'jpg|png|jpeg',
				'max_size'=>2086,
				'file_name'=>$filename
			);
			$config2 = array(
				'upload_path'=>'upload/config_tentang/',
				'allowed_types'=>'jpg|png|jpeg',
				'max_size'=>2086,
				'file_name'=>$filename2
			);
			$this->upload->initialize($config);
			$this->upload->do_upload('gambar');
			$finfo = $this->upload->data();
			$cover = $finfo['file_name'];

			$this->upload->initialize($config2);
			$this->upload->do_upload('gambar2');
			$finfo = $this->upload->data();
			$foto = $finfo['file_name'];

    		$data = array(
	    		'judul_halaman' => $this->input->post('judul_halaman'),
	    		'cover_halaman' => $cover,
	    		'judul_tentang' => $this->input->post('judul_tentang'),
	    		'isi_tentang' => $this->input->post('isi_tentang'),
	    		'nama_quotes' => $this->input->post('nama_quotes'),
	    		'isi_quotes' => $this->input->post('isi_quotes'),
	    		'foto_perusahaan' => $foto,
	    		'id_user' => $this->session->userdata('id'),
	    		'created_date' => date("Y-m-d H:i:s"),
	    		'updated_date' => date("Y-m-d H:i:s")
	    	);
            $this->m_jsit->create_tentang($data);
            $this->session->set_flashdata('notif','<div class="alert alert-success alert-dismissible"><strong> Data berhasil disimpan !</strong><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button></div>');
			redirect('config_tentang/form');
        } else {
        	$config = array(
				'upload_path'=>'upload/config_tentang/',
				'allowed_types'=>'jpg|png|jpeg',
				'max_size'=>2086,
				'file_name'=>$filename
			);

			$config2 = array(
				'upload_path'=>'upload/config_tentang/',
				'allowed_types'=>'jpg|png|jpeg',
				'max_size'=>2086,
				'file_name'=>$filename2
			);

			$this->upload->initialize($config);
			if($this->upload->do_upload('gambar')){
				$finfo = $this->upload->data();
				$gambar_edit = $finfo['file_name'];

				$data_edit = array(
					'judul_halaman' => $this->input->post('judul_halaman'),
					'cover_halaman' => $gambar_edit,
					'judul_tentang' => $this->input->post('judul_tentang'),
					'isi_tentang' => $this->input->post('isi_tentang'),
					'nama_quotes' => $this->input->post('nama_quotes'),
					'isi_quotes' => $this->input->post('isi_quotes'),
					'id_user' => $this->session->userdata('id'),
					'updated_date' => date("Y-m-d H:i:s")
				);

				$kode_id = array('id_ct'=>$id);
				$gambar_db = $this->db->get_where('config_tentang',$kode_id);
				if($gambar_db->num_rows()>0){
					$pros=$gambar_db->row();
					$name_gambar=$pros->cover_halaman;
		
					if(file_exists($lok=FCPATH.'upload/config_tentang/'.$name_gambar)){
					unlink($lok);
					}
				}
			}
			$this->upload->initialize($config2);
			if($this->upload->do_upload('gambar2')){
				$finfo = $this->upload->data();
				$gambar_edit2 = $finfo['file_name'];

				$data_edit = array(
					'judul_halaman' => $this->input->post('judul_halaman'),
					'foto_perusahaan' => $gambar_edit2,
					'judul_tentang' => $this->input->post('judul_tentang'),
					'isi_tentang' => $this->input->post('isi_tentang'),
					'nama_quotes' => $this->input->post('nama_quotes'),
					'isi_quotes' => $this->input->post('isi_quotes'),
					'id_user' => $this->session->userdata('id'),
					'updated_date' => date("Y-m-d H:i:s")
				);

				$kode_id = array('id_ct'=>$id);
				$gambar_db = $this->db->get_where('config_tentang',$kode_id);
				if($gambar_db->num_rows()>0){
					$pros=$gambar_db->row();
					$name_gambar2=$pros->foto_perusahaan;
		
					if(file_exists($lok=FCPATH.'upload/config_tentang/'.$name_gambar2)){
					unlink($lok);
					}
				}
			}
			$this->upload->initialize($config);
			if($this->upload->do_upload('gambar')){
				$finfo = $this->upload->data();
				$gambar_edit = $finfo['file_name'];

				$this->upload->initialize($config2);
				if($this->upload->do_upload('gambar2')){
					$finfo = $this->upload->data();
					$gambar_edit2 = $finfo['file_name'];

					$data_edit = array(
						'judul_halaman' => $this->input->post('judul_halaman'),
						'cover_halaman' => $gambar_edit,
						'foto_perusahaan' => $gambar_edit2,
						'judul_tentang' => $this->input->post('judul_tentang'),
						'isi_tentang' => $this->input->post('isi_tentang'),
						'nama_quotes' => $this->input->post('nama_quotes'),
						'isi_quotes' => $this->input->post('isi_quotes'),
						'id_user' => $this->session->userdata('id'),
						'updated_date' => date("Y-m-d H:i:s")
					);

					$kode_id = array('id_ct'=>$id);
					$gambar_db = $this->db->get_where('config_tentang',$kode_id);
					if($gambar_db->num_rows()>0){
						$pros=$gambar_db->row();
						$name_gambar=$pros->cover_halaman;
						$name_gambar2=$pros->foto_perusahaan;
			
						if(file_exists($lok=FCPATH.'upload/config_tentang/'.$name_gambar)){
							unlink($lok);
						}
						if(file_exists($lok=FCPATH.'upload/config_tentang/'.$name_gambar2)){
							unlink($lok);
						}
					}
				}
			}
			if($this->upload->do_upload('gambar')==null and $this->upload->do_upload('gambar2')==null){
				$data_edit = array(
					'judul_halaman' => $this->input->post('judul_halaman'),
					'judul_tentang' => $this->input->post('judul_tentang'),
					'isi_tentang' => $this->input->post('isi_tentang'),
					'nama_quotes' => $this->input->post('nama_quotes'),
					'isi_quotes' => $this->input->post('isi_quotes'),
					'id_user' => $this->session->userdata('id'),
					'updated_date' => date("Y-m-d H:i:s")
				);
			}
            $this->m_jsit->update_tentang($id,$data_edit);
            $this->session->set_flashdata('notif','<div class="alert alert-success alert-dismissible"><strong> Data berhasil diubah !</strong><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button></div>');
			redirect('config_tentang');
        }
	}

	public function delete($id) {
		$this->m_jsit->delete_tentang($id);
		$this->session->set_flashdata('notif','<div class="alert alert-success alert-dismissible"><strong> Data berhasil dihapus !</strong><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button></div>');
		redirect('config_tentang');
	}
}
