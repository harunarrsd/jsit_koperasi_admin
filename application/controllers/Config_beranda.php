<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Config_beranda extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->library('upload');
		$this->load->model('m_jsit');
		if ($this->session->userdata('udhmasuk')==false) {
			redirect('.');
		}
	}

	public function index()	{
		$data['title'] = 'Koperasi JSIT';
		$data['sql'] = $this->m_jsit->read_beranda();
		$data['sidebar'] = $this->load->view('layouts/sidebar','',true);
        $data['pages'] = $this->load->view('pages/v_config_beranda',array('main'=>$data),true);
		$this->load->view('master',array('main'=>$data));
	}

	public function form() {
		$data['title'] = 'Koperasi JSIT';
		$data['op'] = 'tambah';
		$data['sidebar'] = $this->load->view('layouts/sidebar','',true);
        $data['pages'] = $this->load->view('pages/form/vf_config_beranda',array('main'=>$data),true);
		$this->load->view('master',array('main'=>$data));
	}

	public function form_edit($id) {
		$data['title'] = 'Koperasi JSIT';
		$data['op'] = 'edit';
		$data['sql'] = $this->m_jsit->edit_beranda($id);
		$data['sidebar'] = $this->load->view('layouts/sidebar','',true);
        $data['pages'] = $this->load->view('pages/form/vf_config_beranda',array('main'=>$data),true);
		$this->load->view('master',array('main'=>$data));
	}

	function create() {
		$op = $this->input->post('op');
		$id = $this->input->post('id');
		$filename = date("YmdHis");
    	if ($op=="tambah") {
    		$config = array(
				'upload_path'=>'upload/config_beranda/',
				'allowed_types'=>'jpg|png|jpeg',
				'max_size'=>2086,
				'file_name'=>$filename
			);
			$this->upload->initialize($config);
			$this->upload->do_upload('gambar');
			$finfo = $this->upload->data();
			$background = $finfo['file_name'];

    		$data = array(
	    		'caption_satu' => $this->input->post('caption_satu'),
	    		'caption_dua' => $this->input->post('caption_dua'),
	    		'background' => $background,
	    		'text_button' => $this->input->post('text_button'),
	    		'id_user' => $this->session->userdata('id'),
	    		'created_date' => date("Y-m-d H:i:s"),
	    		'updated_date' => date("Y-m-d H:i:s")
	    	);
            $this->m_jsit->create_beranda($data);
            $this->session->set_flashdata('notif','<div class="alert alert-success alert-dismissible"><strong> Data berhasil disimpan !</strong><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button></div>');
			redirect('config_beranda/form');
        } else {
        	$config = array(
				'upload_path'=>'upload/config_beranda/',
				'allowed_types'=>'jpg|png|jpeg',
				'max_size'=>2086,
				'file_name'=>$filename
			);

			$this->upload->initialize($config);
			if($this->upload->do_upload('gambar')){
				$finfo = $this->upload->data();
				$gambar_edit = $finfo['file_name'];

				$data_edit = array(
		    		'caption_satu' => $this->input->post('caption_satu'),
					'caption_dua' => $this->input->post('caption_dua'),
					'background' => $gambar_edit,
					'text_button' => $this->input->post('text_button'),
					'id_user' => $this->session->userdata('id'),
					'updated_date' => date("Y-m-d H:i:s")
		    	);

				$kode_id = array('id_cb'=>$id);
				$gambar_db = $this->db->get_where('config_beranda',$kode_id);
				if($gambar_db->num_rows()>0){
					$pros=$gambar_db->row();
					$name_gambar=$pros->background;
	
					if(file_exists($lok=FCPATH.'upload/config_beranda/'.$name_gambar)){
					  unlink($lok);
					}
				}
			}else{
				$data_edit = array(
					'caption_satu' => $this->input->post('caption_satu'),
					'caption_dua' => $this->input->post('caption_dua'),
					'text_button' => $this->input->post('text_button'),
					'id_user' => $this->session->userdata('id'),
					'updated_date' => date("Y-m-d H:i:s")
		    	);
			}
            $this->m_jsit->update_beranda($id,$data_edit);
            $this->session->set_flashdata('notif','<div class="alert alert-success alert-dismissible"><strong> Data berhasil diubah !</strong><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button></div>');
			redirect('config_beranda');
        }
	}

	public function delete($id) {
		$this->m_jsit->delete_beranda($id);
		$this->session->set_flashdata('notif','<div class="alert alert-success alert-dismissible"><strong> Data berhasil dihapus !</strong><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button></div>');
		redirect('config_beranda');
	}
}
