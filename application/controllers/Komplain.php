<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Komplain extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->library('upload');
		$this->load->helper('text');
		$this->load->model('m_jsit');
		if ($this->session->userdata('udhmasuk')==false) {
			redirect('.');
		}
	}

	public function index()	{
		$data['title'] = 'Koperasi JSIT';
		$data['sidebar'] = $this->load->view('layouts/sidebar','',true);
        $data['pages'] = $this->load->view('pages/v_komplain',array('main'=>$data),true);
		$this->load->view('master',array('main'=>$data));
	}

	function data_komplain(){
		$data=$this->m_jsit->read_komplain()->result();
		echo json_encode($data);
	}
	
}
