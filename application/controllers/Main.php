<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Main extends CI_Controller {

	function __construct()
	{
        parent::__construct();
		$this->load->model('m_jsit');
	}

	public function index()	{
		$data['title'] = 'Koperasi JSIT';
        $data['pages'] = $this->load->view('pages/v_login','',true);
		$this->load->view('main_login',array('main'=>$data));
	}

	// login
	function ceklogin(){
		if (isset($_POST['login'])) {
			$user=$this->input->post('username',true);
			$pass=md5($this->input->post('password',true));
			$cek=$this->m_jsit->proseslogin($user, $pass);
			$hasil=count($cek);
			if ($hasil > 0) {
				$yglogin=$this->db->get_where('user',array('username'=>$user, 'password'=>$pass))->row();
				$data = array('udhmasuk' => true,
				'id'=>$yglogin->id_user,
				'nama' => $yglogin->nama_user,
				'role' => $yglogin->role);
				$this->session->set_userdata($data);
				redirect('home');
			}else {
				$this->session->set_flashdata('notif','<div class="alert alert-success alert-dismissible"><strong> Maaf Email atau Password Anda Salah ! </strong><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button></div>');
				redirect('.');
			}
		}
	}

	// logout
	function keluar(){
		$this->session->sess_destroy();
		redirect('.');
	}
}
