<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	function __construct()
	{
        parent::__construct();
		$this->load->model('m_jsit');
		if ($this->session->userdata('udhmasuk')==false) {
			redirect('.');
		}
	}

	public function index()	{
		$data['title'] = 'Koperasi JSIT';
		$data['sidebar'] = $this->load->view('layouts/sidebar','',true);
        $data['pages'] = $this->load->view('pages/v_home_penerbit','',true);
		$this->load->view('master',array('main'=>$data));
	}
}
