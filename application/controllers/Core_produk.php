<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Core_produk extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->library('upload');
		$this->load->helper('text');
		$this->load->model('m_jsit');
		if ($this->session->userdata('udhmasuk')==false) {
			redirect('.');
		}
	}

	public function kelas()	{
		$data['title'] = 'Koperasi JSIT';
		$data['sql'] = $this->m_jsit->read_kelas();
		$data['sidebar'] = $this->load->view('layouts/sidebar','',true);
        $data['pages'] = $this->load->view('pages/v_kelas',array('main'=>$data),true);
		$this->load->view('master',array('main'=>$data));
	}

	public function kategori()	{
		$data['title'] = 'Koperasi JSIT';
		$data['sql'] = $this->m_jsit->read_kategori();
		$data['sidebar'] = $this->load->view('layouts/sidebar','',true);
        $data['pages'] = $this->load->view('pages/v_kategori',array('main'=>$data),true);
		$this->load->view('master',array('main'=>$data));
	}

	public function form_kelas() {
		$data['title'] = 'Koperasi JSIT';
		$data['op'] = 'tambah';
		$data['sidebar'] = $this->load->view('layouts/sidebar','',true);
        $data['pages'] = $this->load->view('pages/form/vf_kelas',array('main'=>$data),true);
		$this->load->view('master',array('main'=>$data));
	}

	public function form_kategori() {
		$data['title'] = 'Koperasi JSIT';
		$data['op'] = 'tambah';
		$data['sidebar'] = $this->load->view('layouts/sidebar','',true);
        $data['pages'] = $this->load->view('pages/form/vf_kategori',array('main'=>$data),true);
		$this->load->view('master',array('main'=>$data));
	}

	public function form_edit_kelas($id) {
		$data['title'] = 'Koperasi JSIT';
		$data['op'] = 'edit';
		$data['sql'] = $this->m_jsit->edit_kelas($id);
		$data['sidebar'] = $this->load->view('layouts/sidebar','',true);
        $data['pages'] = $this->load->view('pages/form/vf_kelas',array('main'=>$data),true);
		$this->load->view('master',array('main'=>$data));
	}

	public function form_edit_kategori($id) {
		$data['title'] = 'Koperasi JSIT';
		$data['op'] = 'edit';
		$data['sql'] = $this->m_jsit->edit_kategori($id);
		$data['sidebar'] = $this->load->view('layouts/sidebar','',true);
        $data['pages'] = $this->load->view('pages/form/vf_kategori',array('main'=>$data),true);
		$this->load->view('master',array('main'=>$data));
	}

	function create_kelas() {
		$op = $this->input->post('op');
		$id = $this->input->post('id');
    	if ($op=="tambah") {
    		$data = array(
	    		'nama_kelas' => $this->input->post('nama_kelas')
	    	);
            $this->m_jsit->create_kelas($data);
            $this->session->set_flashdata('notif','<div class="alert alert-success alert-dismissible"><strong> Data berhasil disimpan !</strong><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button></div>');
			redirect('core_produk/form_kelas');
        } else {
        	$data_edit = array(
	    		'nama_kelas' => $this->input->post('nama_kelas')
	    	);
            $this->m_jsit->update_kelas($id,$data_edit);
            $this->session->set_flashdata('notif','<div class="alert alert-success alert-dismissible"><strong> Data berhasil diubah !</strong><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button></div>');
			redirect('core_produk/kelas');
        }
	}

	function create_kategori() {
		$op = $this->input->post('op');
		$id = $this->input->post('id');
    	if ($op=="tambah") {
    		$data = array(
	    		'nama_kategori' => $this->input->post('nama_kategori')
	    	);
            $this->m_jsit->create_kategori($data);
            $this->session->set_flashdata('notif','<div class="alert alert-success alert-dismissible"><strong> Data berhasil disimpan !</strong><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button></div>');
			redirect('core_produk/form_kategori');
        } else {
        	$data_edit = array(
	    		'nama_kategori' => $this->input->post('nama_kategori')
	    	);
            $this->m_jsit->update_kategori($id,$data_edit);
            $this->session->set_flashdata('notif','<div class="alert alert-success alert-dismissible"><strong> Data berhasil diubah !</strong><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button></div>');
			redirect('core_produk/kategori');
        }
	}

	public function delete_kelas($id) {
		$this->m_jsit->delete_kelas($id);
		$this->session->set_flashdata('notif','<div class="alert alert-success alert-dismissible"><strong> Data berhasil dihapus !</strong><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button></div>');
		redirect('core_produk/kelas');
	}

	public function delete_kategori($id) {
		$this->m_jsit->delete_kategori($id);
		$this->session->set_flashdata('notif','<div class="alert alert-success alert-dismissible"><strong> Data berhasil dihapus !</strong><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button></div>');
		redirect('core_produk/kategori');
	}


}
