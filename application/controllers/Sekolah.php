<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sekolah extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->library('upload');
		$this->load->helper('text');
		$this->load->model('m_jsit');
		if ($this->session->userdata('udhmasuk')==false) {
			redirect('.');
		}
	}

	public function member()	{
		$data['title'] = 'Koperasi JSIT';
		// $data['sql'] = $this->m_jsit->read_kontak();
		$data['sidebar'] = $this->load->view('layouts/sidebar','',true);
        $data['pages'] = $this->load->view('pages/v_member',array('main'=>$data),true);
		$this->load->view('master',array('main'=>$data));
	}

	public function non_member()	{
		$data['title'] = 'Koperasi JSIT';
		// $data['sql'] = $this->m_jsit->read_kontak();
		$data['sidebar'] = $this->load->view('layouts/sidebar','',true);
        $data['pages'] = $this->load->view('pages/v_non_member',array('main'=>$data),true);
		$this->load->view('master',array('main'=>$data));
	}

	public function form() {
		$data['title'] = 'Koperasi JSIT';
		$data['op'] = 'tambah';
		$data['sidebar'] = $this->load->view('layouts/sidebar','',true);
        $data['pages'] = $this->load->view('pages/form/vf_config_kontak',array('main'=>$data),true);
		$this->load->view('master',array('main'=>$data));
	}

	public function form_edit($id) {
		$data['title'] = 'Koperasi JSIT';
		$data['op'] = 'edit';
		$data['sql'] = $this->m_jsit->edit_kontak($id);
		$data['sidebar'] = $this->load->view('layouts/sidebar','',true);
        $data['pages'] = $this->load->view('pages/form/vf_config_kontak',array('main'=>$data),true);
		$this->load->view('master',array('main'=>$data));
	}

	function create() {
		$op = $this->input->post('op');
		$id = $this->input->post('id');
		$filename = "cover"."_".date("YmdHis");
    	if ($op=="tambah") {
    		$config = array(
				'upload_path'=>'upload/config_kontak/',
				'allowed_types'=>'jpg|png|jpeg',
				'max_size'=>2086,
				'file_name'=>$filename
			);
			$this->upload->initialize($config);
			$this->upload->do_upload('gambar');
			$finfo = $this->upload->data();
			$cover = $finfo['file_name'];

    		$data = array(
	    		'judul_halaman' => $this->input->post('judul_halaman'),
	    		'cover_halaman' => $cover,
	    		'link_maps' => $this->input->post('link_maps'),
	    		'id_user' => $this->session->userdata('id'),
	    		'created_date' => date("Y-m-d H:i:s"),
	    		'updated_date' => date("Y-m-d H:i:s")
	    	);
            $this->m_jsit->create_kontak($data);
            $this->session->set_flashdata('notif','<div class="alert alert-success alert-dismissible"><strong> Data berhasil disimpan !</strong><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button></div>');
			redirect('config_kontak/form');
        } else {
        	$config = array(
				'upload_path'=>'upload/config_kontak/',
				'allowed_types'=>'jpg|png|jpeg',
				'max_size'=>2086,
				'file_name'=>$filename
			);

			$this->upload->initialize($config);
			if($this->upload->do_upload('gambar')){
				$finfo = $this->upload->data();
				$gambar_edit = $finfo['file_name'];

				$data_edit = array(
					'judul_halaman' => $this->input->post('judul_halaman'),
					'cover_halaman' => $gambar_edit,
					'link_maps' => $this->input->post('link_maps'),
					'id_user' => $this->session->userdata('id'),
					'updated_date' => date("Y-m-d H:i:s")
				);

				$kode_id = array('id_ck'=>$id);
				$gambar_db = $this->db->get_where('config_kontak',$kode_id);
				if($gambar_db->num_rows()>0){
					$pros=$gambar_db->row();
					$name_gambar=$pros->cover_halaman;
		
					if(file_exists($lok=FCPATH.'upload/config_kontak/'.$name_gambar)){
						unlink($lok);
					}
				}
			}
			if($this->upload->do_upload('gambar')==null){
				$data_edit = array(
					'judul_halaman' => $this->input->post('judul_halaman'),
					'link_maps' => $this->input->post('link_maps'),
					'id_user' => $this->session->userdata('id'),
					'updated_date' => date("Y-m-d H:i:s")
				);
			}
            $this->m_jsit->update_kontak($id,$data_edit);
            $this->session->set_flashdata('notif','<div class="alert alert-success alert-dismissible"><strong> Data berhasil diubah !</strong><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button></div>');
			redirect('config_kontak');
        }
	}

	public function delete($id) {
		$this->m_jsit->delete_kontak($id);
		$this->session->set_flashdata('notif','<div class="alert alert-success alert-dismissible"><strong> Data berhasil dihapus !</strong><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button></div>');
		redirect('config_kontak');
	}
}
