<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Produk extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->library('upload');
		$this->load->helper('text');
		$this->load->model('m_jsit');
		if ($this->session->userdata('udhmasuk')==false) {
			redirect('.');
		}
	}

	public function index()	{
		$data['title'] = 'Koperasi JSIT';
		$data['sql'] = $this->m_jsit->read_produk();
		$data['kelas'] = $this->m_jsit->read_kelas();
		$data['kategori'] = $this->m_jsit->read_kategori();
		$data['sidebar'] = $this->load->view('layouts/sidebar','',true);
        $data['pages'] = $this->load->view('pages/v_produk',array('main'=>$data),true);
		$this->load->view('master',array('main'=>$data));
	}

	public function form() {
		$data['title'] = 'Koperasi JSIT';
		$data['op'] = 'tambah';
		$data['kelas'] = $this->m_jsit->read_kelas();
		$data['kategori'] = $this->m_jsit->read_kategori();
		$data['sidebar'] = $this->load->view('layouts/sidebar','',true);
        $data['pages'] = $this->load->view('pages/form/vf_produk',array('main'=>$data),true);
		$this->load->view('master',array('main'=>$data));
	}

	public function form_edit($id) {
		$data['title'] = 'Koperasi JSIT';
		$data['op'] = 'edit';
		$data['sql'] = $this->m_jsit->edit_produk($id);
		$data['kelas'] = $this->m_jsit->read_kelas();
		$data['kategori'] = $this->m_jsit->read_kategori();
		$data['sidebar'] = $this->load->view('layouts/sidebar','',true);
        $data['pages'] = $this->load->view('pages/form/vf_produk',array('main'=>$data),true);
		$this->load->view('master',array('main'=>$data));
	}

	function create() {
		$op = $this->input->post('op');
		$id = $this->input->post('id');
		$filename = date("YmdHis");
    	if ($op=="tambah") {
    		$config = array(
				'upload_path'=>'upload/produk/',
				'allowed_types'=>'jpg|png|jpeg',
				'max_size'=>2086,
				'file_name'=>$filename
			);
			$this->upload->initialize($config);
			$this->upload->do_upload('gambar');
			$finfo = $this->upload->data();
			$cover = $finfo['file_name'];

    		$data = array(
	    		'isbn_produk' => $this->input->post('isbn_produk'),
	    		'foto_produk' => $cover,
	    		'judul_produk' => $this->input->post('judul_produk'),
	    		'harga_produk' => $this->input->post('harga_produk'),
	    		'stok' => 0,
	    		'spesifikasi' => $this->input->post('spesifikasi'),
	    		'id_kelas' => $this->input->post('id_kelas'),
	    		'id_kategori' => $this->input->post('id_kategori'),
	    		'id_user' => $this->session->userdata('id'),
	    		'created_date' => date("Y-m-d H:i:s"),
	    		'updated_date' => date("Y-m-d H:i:s")
	    	);
            $this->m_jsit->create_produk($data);
            $this->session->set_flashdata('notif','<div class="alert alert-success alert-dismissible"><strong> Data berhasil disimpan !</strong><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button></div>');
			redirect('produk/form');
        // } else if($op=="edit_stok"){
		// 	$data_edit = array(
		// 		'stok' => $this->input->post('stok')
		// 	);
		// 	$this->m_jsit->update_produk($id,$data_edit);
        //     $this->session->set_flashdata('notif','<div class="alert alert-success alert-dismissible"><strong> Data berhasil diubah !</strong><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button></div>');
		// 	redirect('produk');
		}else{
			if($this->session->userdata('role') == '3'){
		        $data_edit = array(
    				'stok' => $this->input->post('stok')
    			);
		    }else{
				$config = array(
					'upload_path'=>'upload/produk/',
					'allowed_types'=>'jpg|png|jpeg',
					'max_size'=>2086,
					'file_name'=>$filename
				);

				$this->upload->initialize($config);
				if($this->upload->do_upload('gambar')){
					$finfo = $this->upload->data();
					$gambar_edit = $finfo['file_name'];

					$data_edit = array(
						'isbn_produk' => $this->input->post('isbn_produk'),
						'foto_produk' => $gambar_edit,
						'judul_produk' => $this->input->post('judul_produk'),
						'harga_produk' => $this->input->post('harga_produk'),
						'spesifikasi' => $this->input->post('spesifikasi'),
						'id_kelas' => $this->input->post('id_kelas'),
						'id_kategori' => $this->input->post('id_kategori'),
						'id_user' => $this->session->userdata('id'),
						'updated_date' => date("Y-m-d H:i:s")
					);

					$kode_id = array('id_produk'=>$id);
					$gambar_db = $this->db->get_where('produk',$kode_id);
					if($gambar_db->num_rows()>0){
						$pros=$gambar_db->row();
						$name_gambar=$pros->foto_produk;
			
						if(file_exists($lok=FCPATH.'upload/produk/'.$name_gambar)){
							unlink($lok);
						}
					}
				}
				if($this->upload->do_upload('gambar')==null){
					$data_edit = array(
						'isbn_produk' => $this->input->post('isbn_produk'),
						'judul_produk' => $this->input->post('judul_produk'),
						'harga_produk' => $this->input->post('harga_produk'),
						'spesifikasi' => $this->input->post('spesifikasi'),
						'id_kelas' => $this->input->post('id_kelas'),
						'id_kategori' => $this->input->post('id_kategori'),
						'id_user' => $this->session->userdata('id'),
						'updated_date' => date("Y-m-d H:i:s")
					);
				}
			}
            $this->m_jsit->update_produk($id,$data_edit);
            $this->session->set_flashdata('notif','<div class="alert alert-success alert-dismissible"><strong> Data berhasil diubah !</strong><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button></div>');
			redirect('produk');
        }
	}

	public function delete($id) {
		$this->m_jsit->delete_produk($id);
		$this->session->set_flashdata('notif','<div class="alert alert-success alert-dismissible"><strong> Data berhasil dihapus !</strong><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button></div>');
		redirect('produk');
	}
}
